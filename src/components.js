import Alpine from 'alpinejs'

import {SERVER_URL, SEEKER_ROLE, HIDER_ROLE} from './params.js'
import * as MazeCanvas from './canvas.js'
 
window.Alpine = Alpine

/**
 * All registered Alpine.js components
 */


document.addEventListener('alpine:init', () => {

    // keep in memory current game information 
    // Store and update whole object does not affect front
    // hence use of individual store values
    Alpine.store('gameId', undefined),
        Alpine.store('gameHider', undefined),
        Alpine.store('gameSeeker', undefined),
        Alpine.store('gameWinner', undefined),
        Alpine.store('gameRound', 0),
        Alpine.store('gameMaxRounds', 0),
        Alpine.store('gameEnd', false),
        Alpine.store('gameProgress', 0),
        Alpine.store('gameError', false),
        Alpine.store('gameInfo', "Welcome! The platform is waiting for the creation of a game!"),
        Alpine.store('gameFoundHiders', undefined),
        Alpine.store('gameBlocCasse', undefined)
        Alpine.store('gameBlocDeplace', undefined),
        Alpine.store('gameServer', SERVER_URL),

        // Not possible to update front using this way...
        Alpine.store('game', () => ({
            id: undefined,
            hider: undefined,
            seeker: undefined,
            winner: undefined,
            end: undefined,
            progress: undefined,
            error: false,
            info: 'Waiting game creation...',

            init() {
                Alpine.effect(() => {
                    console.log(this.info);
                });
            }
        })),
        Alpine.data('clientsForm', () => ({

            url: `${SERVER_URL}/clients`,
            clients: [],

            create(e) {

                e.preventDefault();

                // TODO: Improve if possible (cannot use select alpine x-model with object...)
                // currently use of x-bind: on data attributes...
                let selectSeeker = document.getElementById("seekerFormControlSelect");
                let selectHider = document.getElementById("hiderFormControlSelect");

                // disable run of game without knowing clients
                if (selectSeeker.options.length <= 0 || selectHider.options.length <= 0)
                    return;

                let seeker = selectSeeker.options[selectSeeker.selectedIndex];
                let hider = selectHider.options[selectHider.selectedIndex];
                fetch(`${SERVER_URL}/game/create`, {
                    method: 'POST',
                    body: JSON.stringify({
                        "seeker": {
                            "port": parseInt(seeker.getAttribute("data-port")),
                            "address": seeker.getAttribute("data-address"),
                            "role": parseInt(seeker.getAttribute("data-role"))
                        },
                        "hider": {
                            "port": parseInt(hider.getAttribute("data-port")),
                            "address": hider.getAttribute("data-address"),
                            "role": parseInt(hider.getAttribute("data-role"))
                        }
                    })
                }).then(res => {
                    // check for error response
                    if (!res.ok) {
                        // get error message from body or default to response status
                        const error = res.status;
                        return Promise.reject(error);
                    }

                    return res.json();

                }).then(data => {

                    Alpine.store('gameId', data["id"]);
                    Alpine.store('gameEnd', false);
                    Alpine.store('gameError', false);
                    Alpine.store('gameWinner', undefined);
                    MazeCanvas.load();
                });
            },
            getSeekers() {
                return this.clients.filter(cli => cli.role == SEEKER_ROLE)
            },
            getHiders() {
                return this.clients.filter(cli => cli.role == HIDER_ROLE)
            },
            canCreate() {
                return this.getSeekers() > 0 && this.getHiders() > 0;
            },
            nSeekers() {
                return this.getSeekers().length;
            },
            nHiders() {
                return this.getHiders().length;
            },


        }))

    Alpine.data('gameControl', () => ({

        runState: true,
        canvasInterval: undefined,

        launchGame() {
            if (!Alpine.store('gameEnd') && !Alpine.store('gameError')) {
                MazeCanvas.updateLoop()
            } else {
                clearInterval(this.canvasInterval);
                this.runState = true; // by default
            }
        },
        step() {

            if (this.runState) {
                this.canvasInterval = setInterval(_ => this.launchGame(), 800);
            } else {
                clearInterval(this.canvasInterval);
            }

            this.runState = !this.runState;
        },
        btnText() {
            return this.runState ? 'Run' : 'Pause';
        },
        btnClass() {
            return this.runState ? 'btn btn-primary' : 'btn btn-warning';
        }
    }));

    Alpine.data('progressComponent', () => ({

        getStyle() {
            return `width: ${Alpine.store('gameProgress')}%`
        }
    }));
    let indice = undefined;
    Alpine.data('FoundHiders', () => ({
        result() {
            if (indice === undefined) {
                indice = Alpine.store('gameFoundHiders');
            }
            return indice - Alpine.store('gameFoundHiders')
        }
    }));
    Alpine.data('Blocs', () => ({
        bloc_casse(){
            return Alpine.store('gameBlocCasse');
        },
        bloc_deplace(){
            return Alpine.store('gameBlocDeplace');
        }
    }));




});
    Alpine.start()


    Alpine.start()



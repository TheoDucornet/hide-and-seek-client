import ThreeMaze from './maze.js'
import Alpine from 'alpinejs'
import {SERVER_URL} from './params.js'
import {Chart, registerables} from "chart.js";
Chart.register(...registerables);

var maze = undefined;
var indice = 0;
var bloc_casse = 0;
var bloc_deplace = 0;
var ListeDistance = undefined;
let DestroyOrNot = 0;
var InfoRound = undefined;
let rnd = undefined;
let liste = undefined;
let chart = undefined;

const wrapper = document.querySelector('.three');


const load = () => {
    
    let id = Alpine.store('gameId');


    fetch(`${SERVER_URL}/game?id=${id}`)
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = response.status;
                return Promise.reject(error);
            }

            // remove info message (if exists)
            Alpine.store('gameInfo', "");
            
            return res.json();
        })
        .then(data => {
            
            // initialize game
            wrapper.innerHTML = ""
            Alpine.store('gameEnd', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);


            let width = data['width']
            let height = data['height']

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);
            
            Alpine.store('gameProgress', percentProgress);

            maze = new ThreeMaze(window, wrapper, width, height);
    
            // Inits
            maze.initScene();
            maze.onWindowResize();
            maze.render();

            var map = [];
            var orient = [];

            var ply = 0;

            var nbrHider = 0;
            var graph = {};

            var ply = 0;
            var nbrHider = 0;
            var nbrSeeker = 0;
            var nbrBlocs = 0;

            let counter = 0;

            for (var x = 1; x <= width; x += 1)
            {
                map[x] = [];

                for (var y = 1; y <= height; y += 1)
                {
                    if ('block' in data['cells'][counter]) {

                        if(data['cells'][counter]["block"]["kindStr"] === "MATERIAL")
                        {
                            map[x][y] = data['cells'][counter]["block"]["materialStr"]
                        }
                        else{
                            map[x][y] = data['cells'][counter]["block"]["kindStr"]
                        }

                        if(data['cells'][counter]["block"]["kind"] === 2) {
                            nbrBlocs += 1;
                            indice += 1;
                        }
                    }
                    if ('player' in data['cells'][counter]) {
                        
                        map[x][y] = data['cells'][counter]["player"]["roleStr"]
                        if(data['cells'][counter]["player"]["roleStr"] === "HIDER"){
                            nbrHider += 1;
                        }
                        orient[ply] = data['cells'][counter]['player']["orientationStr"];
                        ply++;

                        if(data['cells'][counter]["player"]["roleStr"] === "SEEKER"){
                            nbrSeeker += 1;
                        }
                        orient[ply] = data['cells'][counter]['player']["orientationStr"];
                        ply++;

                        graph[data['cells'][counter]['player']["name"]] = data['cells'][counter]["player"]["location"]


                    }
                    counter++;
                }
            }

            var DistanceResult = []
            var Index = 0;

            for(let i=1; i<nbrSeeker+1; i++)
            {
                for(let j=1; j<nbrHider+1; j++)
                {
                    if(graph["H".concat(j)] !== undefined) {
                        let diffx = graph["S".concat(i)]["x"] - graph["H".concat(j)]["x"];
                        if (diffx < 0) {diffx = -diffx};
                        let diffy = graph["S".concat(i)]["y"] - graph["H".concat(j)]["y"];
                        if (diffy < 0) {diffy = -diffy};
                        DistanceResult[Index] = diffx + diffy;
                        Index++;
                    }
                };
            };
            let merge = 0;
            for(var g=0; g<DistanceResult.length; g++){
                merge += DistanceResult[g];
            }
            merge = merge/nbrHider*nbrSeeker;

            ListeDistance = [];
            liste = 0;

            ListeDistance[liste] = merge;
            liste++;

            InfoRound = [];

            rnd = 0;
            InfoRound[rnd] = data['round'];

            let canvas = document.getElementById('myChart');
            let ctx = canvas.getContext('2d');

            if(DestroyOrNot > 0){
                chart.destroy();
            }
            DestroyOrNot++;

            chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: InfoRound,
                    datasets:[{
                        label: "Distance moyennne entre les Seekers et les Hiders",
                        data: ListeDistance,
                        fill: false,
                        borderColor: 'rgb(75,192,192)',
                        tension: 0.1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    },
                    animation: {
                        duration: 0
                    }
                }
            });

            Alpine.store('gameFoundHiders', nbrHider);
            Alpine.store('gameBlocCasse', 0);
            Alpine.store('gameBlocDeplace', 0);

            var bloc_casse = 0;
            var bloc_deplace = 0;

            // load maze map
            maze.orientationPlayer(orient);
            maze.onGenerateMaze(map);

            window.addEventListener('resize', maze.onWindowResize.bind(maze));
        }).catch(e => {
            console.log(e)
            Alpine.store('gameInfo', "Game not found...");
            Alpine.store('gameError', true);
        });
}

const updateLoop = () => {
    
    let id = Alpine.store('gameId');

    fetch(`${SERVER_URL}/game/step`, {
            method: 'POST', 
            body: JSON.stringify({"id": id})
        })
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = res.status;
                return Promise.reject(error);
            }

            // remove error message (if exists)
            Alpine.store('gameInfo', "");
            
            return res.json();
        })
        .then(data => {
            
            // no server error currently
            Alpine.store('gameError', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);


            let width = data['width']
            let height = data['height']
            console.log(data['round'], 'of', data['maxRounds'], '=> end? : ', data['end']);
            let gameEnd = data['end'];

            Alpine.store('gameEnd', gameEnd);

            if (gameEnd) {
                // clearInterval(canvasInterval);
                let currentWinner = data['winnerStr'];
                Alpine.store('gameInfo',`Game ${Alpine.store("gameId")} is finished! Winner is: ${currentWinner}`)
                Alpine.store('gameWinner', currentWinner);
            }

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);

            Alpine.store('gameProgress', percentProgress);

            var map = [];

            var nbrHider = 0;
            var nbrBlocs =0;

            var orient = [];
            var orient = [];
            var InfoBlock = {"STONE": {}, "METAL": {}};
            let stone = 0;
            let metal = 0;

            var graph = {};

            var ply = 0;
            var nbrHider = 0;
            var nbrBlocs =0;
            var nbrSeeker = 0;

            let counter = 0;


            for (var x = 1; x <= width; x += 1)
            {
                map[x] = [];

                for (var y = 1; y <= height; y += 1)
                {   
                    if ('block' in data['cells'][counter]) {

                        if(data['cells'][counter]["block"]["kindStr"] === "MATERIAL") {
                            map[x][y] = data['cells'][counter]["block"]["materialStr"]
                        }
                        else{
                            map[x][y] = data['cells'][counter]["block"]["kindStr"]
                        }

                        if(data['cells'][counter]["block"]["kind"] === 2) {
                            nbrBlocs += 1;
                        }

                        if(data['cells'][counter]["block"]["materialStr"] === "STONE"){
                            if(data['cells'][counter]["block"]["resistance"] === 50){
                                InfoBlock["STONE"][stone] = {"x": x, "y": y};
                                stone++;
                            }
                        }
                        if(data['cells'][counter]["block"]["materialStr"] === "METAL"){
                            if(data['cells'][counter]["block"]["resistance"] === 80){
                                InfoBlock["METAL"][metal] = {"x": x, "y": y, "id": parseInt(x+""+y), "damaged": 80};
                                metal++;
                            } else if(data['cells'][counter]["block"]["resistance"] === 40){
                                for(var z=0; z<metal; z++)
                                {
                                    if(InfoBlock["METAL"][z]["id"] === parseInt(x+""+y)){
                                        InfoBlock["METAL"][z]["damaged"] = 40;
                                    }
                                }
                            }
                        }


                    }

                    if ('player' in data['cells'][counter]) {

                        map[x][y] = data['cells'][counter]["player"]["roleStr"]
                        if(data['cells'][counter]["player"]["roleStr"] === "HIDER"){
                            nbrHider += 1;
                        }
                        orient[ply] = data['cells'][counter]['player']["orientationStr"];
                        ply++;

                        if(data['cells'][counter]["player"]["roleStr"] === "SEEKER"){
                            nbrSeeker += 1;
                        }
                        orient[ply] = data['cells'][counter]['player']["orientationStr"];
                        ply++;

                        graph[data['cells'][counter]['player']["name"]] = data['cells'][counter]["player"]["location"];


                    }
                    counter++;
                }
            }

            if(data['currentAction']['interaction'] === 1)
            {
                if(indice > nbrBlocs){
                    indice = nbrBlocs;
                    bloc_casse += 1;

                    Alpine.store('gameBlocCasse', bloc_casse);
                }
            }else if(data['currentAction']['interaction'] === 2)
            {
                if(indice > nbrBlocs){
                    indice = nbrBlocs;
                    bloc_deplace += 1;

                    Alpine.store('gameBlocDeplace', bloc_deplace);
                }
            }
            indice = nbrBlocs;


            Alpine.store('gameFoundHiders', nbrHider);
            Alpine.store('gameFoundBlocs', nbrBlocs);

            console.log(indice);
            if(data['currentAction']['interaction'] === 1)
            {
                if(indice > nbrBlocs){
                    indice = nbrBlocs;
                    bloc_casse += 1;

                    Alpine.store('gameBlocCasse', bloc_casse);
                }
            } else if(data['currentAction']['interaction'] === 2)
            {
                if(indice > nbrBlocs){
                    indice = nbrBlocs;
                    bloc_deplace += 1;

                    Alpine.store('gameBlocDeplace', bloc_deplace);
                }
            }
            indice = nbrBlocs;

            Alpine.store('gameFoundHiders', nbrHider);
            Alpine.store('gameFoundBlocs', nbrBlocs);

            Alpine.store('gameFoundHiders', nbrHider);
            Alpine.store('gameFoundBlocs', nbrBlocs);

            // reload maze map
            if (maze !== undefined) {
                var DistanceResult = []
                var Index = 0;
            }

            for(let i=1; i<nbrSeeker+1; i++)
            {
                for(let j=1; j<nbrHider+1; j++)
                {
                   if(graph["H".concat(j)] !== undefined) {
                       let diffx = graph["S".concat(i)]["x"] - graph["H".concat(j)]["x"];
                       if (diffx < 0) {diffx = -diffx};
                       let diffy = graph["S".concat(i)]["y"] - graph["H".concat(j)]["y"];
                       if (diffy < 0) {diffy = -diffy};
                       DistanceResult[Index] = diffx + diffy;
                       Index++;
                   }
                };
            };
            let merge = 0;
            for(var g=0; g<DistanceResult.length; g++){
                merge += DistanceResult[g];
            }
            merge = merge/nbrHider*nbrSeeker;
            ListeDistance[liste] = merge;
            liste++;

            if(data['round'] !== rnd){
                rnd++;
                InfoRound[rnd] = data['round'];
            }

            let canvas = document.getElementById('myChart');
            let ctx = canvas.getContext('2d');

            chart.destroy();

            chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: InfoRound,
                    datasets:[{
                        label: "Distance moyennne entre les Seekers et les Hiders",
                        data: ListeDistance,
                        fill: false,
                        borderColor: 'rgb(75,192,192)',
                        tension: 0.1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    },
                    animation: {
                        duration: 0
                    }
                }
            });


            Alpine.store('gameFoundHiders', nbrHider);
            Alpine.store('gameFoundBlocs', nbrBlocs);
            
            // reload maze map
            if (maze !== undefined) {

                maze.InfoBlock(InfoBlock);
                maze.orientationPlayer(orient);
                maze.onGenerateMaze(map);
            }
        })
        .catch((e) => {
            console.log(e)

            Alpine.store('gameInfo', "Server cannot be reached");
            Alpine.store('gameError', true);
        });
}

export {load, updateLoop};
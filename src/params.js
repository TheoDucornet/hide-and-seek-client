// global variable
const SERVER_URL = 'http://127.0.0.1:8080'
const SEEKER_ROLE = 0;
const HIDER_ROLE = 1;

export {SERVER_URL, SEEKER_ROLE, HIDER_ROLE};
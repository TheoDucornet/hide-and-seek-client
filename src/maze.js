import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader'
import {MTLLoader} from 'three/examples/jsm/loaders/MTLLoader'

/**
 * Maze class
 * @param wrapper
 * @param button
 */
class ThreeMaze
{
    constructor(window, wrapper, width, height) {
        // Object attributes
        this.window = window
        this.wrapper = wrapper;
        this.camera = {};
        this.scene = {};
        this.materials = {};
        this.map = [];
        this.renderer = {};
        this.end = {};
        this.width = width;
        this.height = height;
        this.thickness = 30;
        this.j = "";
        this.x = 0;
        this.y = 0;
        this.orientation = [];
        this.priority = [];
        this.materiaux = {};

        this.infoblock = {"STONE": {},
                          "METAL": {}};
    }

    generateEmptyMap = (width, height) => {
        var map = [];
    
        for (var x = 1; x <= width; x += 1)
        {
            map[x] = [];
            for (var y = 1; y <= height; y += 1)
            {
                map[x][y] = 0;  
            }
        }
    
        return map;
    }
    
    onGenerateMaze(map_representation)
    {
        var new_map = this.generateEmptyMap(this.width, this.height);
        var ply = 0;

        for (var x = this.width; x > 0; x -= 1)
        {
            for (var y = 1; y < this.height + 1; y += 1)
            {      
                // store current kind of cell
                new_map[x][y] = {
                    'kind': map_representation[x][y] // block or player
                }
                // check if necessary to update (need to be improved => some blocks can be damaged)
                if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined' 
                    && new_map[x][y].kind === this.map[x][y].kind) {
                    if(new_map[x][y].kind === "STONE" || new_map[x][y].kind === "METAL")
                    {
                        if(this.infoblock["STONE"][parseInt(x+""+y)] !== undefined || this.infoblock["METAL"][parseInt(x+""+y)] !== undefined){
                            if(this.map[x][y].materiaux !== undefined){
                                this.map[x][y].materiaux.visible = false;
                                this.map[x][y].materiaux.material.transparent = false;
                                this.map[x][y].mesh.visible = false;
                                this.scene.remove(this.map[x][y].mesh);
                                this.scene.remove(this.map[x][y].materiaux);
                            }
                        }else{
                            new_map[x][y] = this.map[x][y];
                            continue;
                        }
                    }else{
                        new_map[x][y] = this.map[x][y];
                        continue;
                    }

                } else if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined') {
                    
                    // check if player and take care in this case of the two mesh
                    if (this.map[x][y].player !== undefined) {
                        
                        this.map[x][y].player.visible = false;
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].player);
                        this.scene.remove(this.map[x][y].mesh);
                    }
                    else if(this.map[x][y].materiaux !== undefined) {
                        this.map[x][y].materiaux.visible = false;
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].mesh);
                        this.scene.remove(this.map[x][y].materiaux);
                    } else if(this.map[x][y].mesh !== undefined){
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].mesh);
                    }
                }

                // Adds a new mesh if needed
                if (map_representation[x][y] === "BORDER")
                {
                    // Generates the mesh for border

                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                
                }
                else if (map_representation[x][y] === "GROUND") {
                     // generate the mesh with ground
                     var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                     new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                     new_map[x][y].mesh.visible = true;
                     new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                     this.scene.add(new_map[x][y].mesh);

                }
                else if (map_representation[x][y] === "HIDER")
                {   
                    // Add hider
                    const GroupHider = new THREE.Group();
                    var Hdir = undefined;
                    var objetDir = undefined;

                    //Rotation
                    if(this.priority[ply] === "TOP"){
                        Hdir = new THREE.Vector3(this.orientation[ply],0,0);
                        objetDir = 55;
                    } else if(this.priority[ply] === "BOTTOM"){
                        Hdir = new THREE.Vector3(this.orientation[ply],0,0);
                        objetDir = 165
                    } else if(this.priority[ply] === "LEFT"){
                        Hdir = new THREE.Vector3(0,0,this.orientation[ply]);
                        objetDir = 110
                    } else if(this.priority[ply] === "RIGHT"){
                        Hdir = new THREE.Vector3(0,0,this.orientation[ply]);
                        objetDir = 0
                    }
                    ply++;
                    const Horigin = new THREE.Vector3(0,0,0);
                    const mtlLoader = new MTLLoader();


                    //Création personnage
                    var self = this;
                    mtlLoader.load('./model/WaltHead.mtl', function(materials){
                        materials.preload();
                        const objLoader = new OBJLoader()
                        objLoader.setMaterials(materials)
                        objLoader.load(
                            './model/WaltHead.obj',
                            (object) => {
                                object.rotation.y = objetDir
                                object.scale.set(0.5, 0.5, 0.5)
                                GroupHider.add(object)
                            },
                             (xhr) => {
                                console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
                            },
                            (error) => {
                                console.log(error)
                            }
                        );
                    });


                    new_map[x][y].player = GroupHider

                    new_map[x][y].player.visible = true;
                    new_map[x][y].player.position.set(x * this.thickness - ((this.width * this.thickness) / 2), this.thickness / 2, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].player);


                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);                    
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                }
                else if (map_representation[x][y] === "SEEKER")
                {

                    //Même chose que pour le Hider

                    const GroupSeeker = new THREE.Group();
                    var Sdir = undefined;
                    var objetDir = undefined;
                    if(this.priority[ply] === "TOP"){
                        Sdir = new THREE.Vector3(this.orientation[ply],0,0);
                        objetDir = 55;
                    } else if(this.priority[ply] === "BOTTOM"){
                        Sdir = new THREE.Vector3(this.orientation[ply],0,0);
                        objetDir = 165;
                    } else if(this.priority[ply] === "LEFT"){
                        Sdir = new THREE.Vector3(0,0,this.orientation[ply]);
                        objetDir = 110;
                    } else if(this.priority[ply] === "RIGHT"){
                        Sdir = new THREE.Vector3(0,0,this.orientation[ply]);
                        objetDir = 0;
                    }
                    ply++;
                    const Sorigin = new THREE.Vector3(0,0,0);


                    var self = this;
                    const objLoader = new OBJLoader()
                    objLoader.load(
                        './model/WaltHead.obj',
                        (object) => {
                            object.rotation.y = objetDir
                            object.scale.set(0.5, 0.5, 0.5)
                            GroupSeeker.add(object)
                        },
                        (xhr) => {
                            console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
                        },
                        (error) => {
                            console.log(error)
                        }
                    )

                    new_map[x][y].player = GroupSeeker

                    new_map[x][y].player.visible = true;
                    new_map[x][y].player.position.set(x * this.thickness - ((this.width * this.thickness) / 2), this.thickness / 2, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].player);

                    // also add the ground mesh
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                } else{

                    if(map_representation[x][y] === "WOOD")
                    {
                        var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                        new_map[x][y].materiaux = new THREE.Mesh(wall_geometry, this.materials.bois);
                        new_map[x][y].materiaux.visible = true;
                        new_map[x][y].materiaux.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].materiaux);

                        var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                        new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                        new_map[x][y].mesh.visible = true;
                        new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].mesh);
                    }
                    else if(map_representation[x][y] === "STONE")
                    {

                        var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                        if(this.infoblock["STONE"][parseInt(x+""+y)] !== undefined){

                            new_map[x][y].materiaux = new THREE.Mesh(wall_geometry, this.materials.pierre_50);
                            new_map[x][y].materiaux.material.transparent = true;
                        }else{
                            new_map[x][y].materiaux = new THREE.Mesh(wall_geometry, this.materials.pierre);
                        }
                        new_map[x][y].materiaux.visible = true;

                        new_map[x][y].materiaux.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].materiaux);

                        var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                        new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                        new_map[x][y].mesh.visible = true;
                        new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].mesh);


                    } else if(map_representation[x][y] === "METAL") {
                        var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                        if(this.infoblock["METAL"][parseInt(x+""+y)] === 80)
                        {
                            new_map[x][y].materiaux = new THREE.Mesh(wall_geometry, this.materials.acier_80);
                            new_map[x][y].materiaux.material.transparent = true;
                        } else if(this.infoblock["METAL"][parseInt(x+""+y)] === 40){
                            new_map[x][y].materiaux = new THREE.Mesh(wall_geometry, this.materials.acier_40);
                            new_map[x][y].materiaux.material.transparent = true;
                        }else{
                            new_map[x][y].materiaux = new THREE.Mesh(wall_geometry, this.materials.acier);
                        }
                        new_map[x][y].materiaux.visible = true;

                        new_map[x][y].materiaux.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].materiaux);

                        var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                        new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                        new_map[x][y].mesh.visible = true;
                        new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].mesh);
                    } else {
                        var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                        new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                        new_map[x][y].mesh.visible = true;
                        new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                        this.scene.add(new_map[x][y].mesh);
                    }

                }

            }
        }
        this.map_representation = map_representation;

        this.map = new_map;
    };
 
    /**
     * Inits the scene
     */
    initScene()
    {
        // Scene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xffffff);

        // Materials
        const loader = new THREE.TextureLoader();
        const model = new GLTFLoader();

        const ground_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://thumbs.dreamstime.com/b/pixel-mine-artisanat-style-land-block-concept-de-jeu-pixellis%C3%A9-fond-carr%C3%A9-sans-couture-illustration-vectorielle-bloc-terre-212305562.jpg'),
        })

        const wall_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://pbs.twimg.com/profile_images/1114588770219720709/MdaMyVs4_400x400.jpg'),
            wireframe: false
        })

        const wood_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://lh3.googleusercontent.com/Hfq6fNIx7sicG2jNinMyks4rriaX0sZXU4OiWZ1EwP-5p1wSNuFQAZp6ETI2uClq9KWyeFuN2koxm4vZcykNPA'),
            wireframe: false
        })

        const cobble_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://c1.alamy.com/thumbs/2ex9g6m/pixel-minecraft-style-pave-de-paves-arriere-plan-concept-de-jeu-pixelated-sans-couture-carre-gris-pierre-arriere-plan-illustration-vectorielle-2ex9g6m.jpg'),
            wireframe: false
        })
        const cobble_material_50 = new THREE.MeshBasicMaterial({
            map: loader.load('https://c1.alamy.com/thumbs/2ex9g6m/pixel-minecraft-style-pave-de-paves-arriere-plan-concept-de-jeu-pixelated-sans-couture-carre-gris-pierre-arriere-plan-illustration-vectorielle-2ex9g6m.jpg'),
            wireframe: false,
            opacity: 0.5
        })

        const iron_material = new THREE.MeshBasicMaterial({
            map: loader.load('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtAlgsV_U8AAyDKbPc7QhHyYDJC7pviBxCcyVbArxUc1z0fHzjzulwZwyzhjRDMtrkv8w&usqp=CAU'),
            wireframe: false
        })
        const iron_material_80 = new THREE.MeshBasicMaterial({
            map: loader.load('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtAlgsV_U8AAyDKbPc7QhHyYDJC7pviBxCcyVbArxUc1z0fHzjzulwZwyzhjRDMtrkv8w&usqp=CAU'),
            wireframe: false,
            opacity: 0.7
        })
        const iron_material_40 = new THREE.MeshBasicMaterial({
            map: loader.load('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtAlgsV_U8AAyDKbPc7QhHyYDJC7pviBxCcyVbArxUc1z0fHzjzulwZwyzhjRDMtrkv8w&usqp=CAU'),
            wireframe: false,
            opacity: 0.2
        })
        
        this.materials =
        {
            grey: wall_material,
            ground: ground_material,
            bois: wood_material,
            pierre: cobble_material,
            pierre_50: cobble_material_50,
            acier: iron_material,
            acier_80: iron_material_80,
            acier_40: iron_material_40,
            orange: new THREE.MeshLambertMaterial({color: 0xbe842b}),
        };

        // Camera
        this.camera = new THREE.PerspectiveCamera(45, 1, 1, 2000);
        this.camera.position.x = 1000;
        this.camera.position.y = 800;
        this.camera.position.z = -300;

        this.camera.clicked = false;

        // Lights
        this.scene.add(new THREE.AmbientLight(0xc9c9c9));
        var directional = new THREE.DirectionalLight(0xc9c9c9, 0.5);
        directional.position.set(0, 0.5, 1);
        this.scene.add(directional);

        // this.camera.lookAt(this.scene.position);
        // Renderer
        this.renderer = typeof WebGLRenderingContext != 'undefined' && window.WebGLRenderingContext ? new THREE.WebGLRenderer({antialias: true}) : new THREE.CanvasRenderer({});
        this.wrapper.appendChild(this.renderer.domElement);
    };

    /**
     * Render loop
     * Sets the camera position and renders the scene
     */
    render()
    {
        requestAnimationFrame(this.render.bind(this));
        
            this.camera.lookAt(new THREE.Vector3(0, -150, 0));
            this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.render(this.scene, this.camera);
    };

    /**
     * Sets the scene dimensions on window resize
     */
    onWindowResize()
    {
        // Check if there is a better way to adjust width and height
        var width = (this.window.innerWidth / 12) * 8.5 || this.window.document.body.clientWidth;
        var height = this.window.innerHeight - 120 || this.window.document.body.clientHeight;
        this.renderer.setSize(width, height);
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    };

    orientationPlayer(orient)
    {
        for(var i=0; i<orient.length; i++) {
            if (orient[i] === "Top") {
                this.priority[i] = "TOP";
                this.orientation[i] = -30;
            } else if (orient[i] === "Bottom") {
                this.priority[i] = "BOTTOM";
                this.orientation[i] = 30;
            } else if (orient[i] === "Left") {
                this.priority[i] = "LEFT";
                this.orientation[i] = -30;
            } else if (orient[i] === "Right") {
                this.priority[i] = "RIGHT";
                this.orientation[i] = 30;
            }
        }
    };

    InfoBlock(coord){
        this.infoblock = {"STONE": {},
            "METAL": {}};

        for (var i = 0; i < Object.keys(coord["STONE"]).length; i++) {
            let x = coord["STONE"][i]["x"];
            let y = coord["STONE"][i]["y"];

            this.infoblock["STONE"][parseInt(x + "" + y)] = {"x": x, "y": y};
        }

        for (var i = 0; i < Object.keys(coord["METAL"]).length; i++) {
            if(coord["METAL"][i]["damaged"] === 80){
                let x = coord["METAL"][i]["x"];
                let y = coord["METAL"][i]["y"];

                this.infoblock["METAL"][parseInt(x + "" + y)] = 80;
            } else if(coord["METAL"][i]["damaged"] === 40){
                let x = coord["METAL"][i]["x"];
                let y = coord["METAL"][i]["y"];

                this.infoblock["METAL"][parseInt(x + "" + y)] = 40;
            }
        }

    };
}



export default ThreeMaze;